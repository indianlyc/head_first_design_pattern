from fly_strategy import FlyWithWings, FlyNoWay
from quack_strategy import Quack, Squeak

class Duck():
    def __init__(self):
        self.fly_behavior = None
        self.quack_behavior = None

    def set_fly_behavior(self, fb):
        self.fly_behavior = fb

    def set_quack_behavior(self, qb):
        self.quack_behavior = qb

    def display(self):
        raise NotImplementedError()

    def perform_fly(self):
        self.fly_behavior.fly()

    def perform_quack(self):
        self.quack_behavior.quack()

    def swim(self):
        print("All ducks float, even decoys!")


class RedHeadDuck(Duck):
    def __init__(self):
        self.fly_behavior = FlyWithWings()
        self.quack_behavior = Quack()

    def display(self):
        print("I'm a real Red Headed duck")


class RubberDuck(Duck):
    def __init__(self):
        self.fly_behavior = FlyNoWay()
        self.quack_behavior = Squeak()

    def display(self):
        print("I'm a rubber duckie")


class MallardDuck(Duck):
    def __init__(self):
        self.fly_behavior = FlyWithWings()
        self.quack_behavior = Squeak()

    def display(self):
        print("I'm a real Mallard duck")


class ModelDuck(Duck):
    def __init__(self):
        self.fly_behavior = FlyNoWay()
        self.quack_behavior = Quack()

    def display(self):
        print("I'm a real model duck")


class DecoyDuck(Duck):
    def __init__(self):
        super(DecoyDuck, self).__init__()
        self.set_fly_behavior(FlyNoWay())
        self.set_quack_behavior(Squeak())

    def display(self):
        print("I'm a duck Decoy")



