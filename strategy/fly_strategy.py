
class FlyBehavior():
    def fly(self):
        raise NotImplementedError()


class FlyNoWay(FlyBehavior):
    def fly(self):
        print("I can't fly")


class FlyRocketPowered(FlyBehavior):
    def fly(self):
        print("I'm flying with a rocket")


class FlyWithWings(FlyBehavior):
    def fly(self):
        print("I'm flying!!")