
class QuackBehavior():
    def quack(self):
        raise NotImplementedError()


class MuteQuack(QuackBehavior):
    def quack(self):
        print("<< Silence >>")


class Quack(QuackBehavior):
    def quack(self):
        print("Quack")


class Squeak(QuackBehavior):
    def quack(self):
        print("Squeak")


class FakeQuack(QuackBehavior):
    def quack(self):
        print("Qwak")