from duck import MallardDuck, RubberDuck, DecoyDuck, ModelDuck
from fly_strategy import FlyRocketPowered


def main():
    mallard = MallardDuck()
    rubber_duckie = RubberDuck()
    decoy = DecoyDuck()

    model = ModelDuck()

    mallard.perform_quack()
    rubber_duckie.perform_quack()
    decoy.perform_quack()

    model.perform_fly()
    model.set_fly_behavior(FlyRocketPowered())
    model.perform_fly()

if __name__ == '__main__':
    main()